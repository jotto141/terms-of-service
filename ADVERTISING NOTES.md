# Federal Advertising Law Notes

- Deceptive advertising

The FTC has in place rules regarding “truth in advertising” which requires the following:

Advertisements must be:

-truthful and non-deceptive
-**fair**
-supported by ​ **evidence**

♦ ​An advertisement is deceptive if it contains a statement that is:

-likely to mislead a consumer who is acting otherwise reasonably

-material, ​meaning that the information contained is pivotal in determining the consumer’s
decision to buy/use the advertised product.

♦ ​An advertisement is unfair if it is:

-likely to cause substantial injury, which could otherwise have been avoided by a
reasonable consumer
-not outweighed by its benefit to the consumer

♦ ​An advertisement is supported by evidence if:

-before the ad is produced, there exists a reasonable basis for the relevant claim to be
made*

*The nature/quality of the evidence may vary per the claim. At minimum, the claimant
must possess the level of evidence it claims to have. Customer testimonials are usually
not enough to meet this standard.

The FTC considers both direct and implied claims in the context of the ad.

- Ambiguous Advertising

Ambiguous advertising generally includes tactics such as the omission of pertinent facts,
exaggerating a product's capabilities or manipulating material terms of a service agreement, for
the purpose of deceptively advertising a good or service to the consumer.

♦ ​Deceptive Trade Practices Act
Every state legislature has enacted some form of a deceptive trade practices act to
prevent retailers and manufacturers from selling products using ambiguous, confusing or
false information to entice consumers. Deceptive trade statutes generally prohibit any

advertising or trade practices that misrepresent, omit, suppress or conceal material
information that a consumer would need to know to make an informed choice.

(See: https://portal.ct.gov/DCP/Trade-Practices-Division/About-the-Connecticut-Unfair-Trade-Pr
actices-Act-CUTPA

For more information on deceptive trade measures taken in Connecticut. This may or
may not be useful for Minds specifically, as we are based in CT. Feel free to discard this
section if need.)

- Federal Trade Commission (FTC) Act

Under Section 5(b) of the FTC Act, the Commission may challenge “unfair or deceptive
act[s] or practice[s],” “unfair methods of competition,” or violations of other laws enforced
through the FTC Act, by instituting an administrative adjudication.

To qualify as a violation of Section 5, an advertisement must meet three criteria:

- ​The advertisement must contain an omission, representation or practice that is likely to
mislead the consumer.
-The advertisement must be considered deceptive from the perspective of a reasonable,
average consumer.
-The omission or representation must be material and not minor or insignificant.

- First Amendment

Advertisements are generally protected under the first amendment, except those which
meet the criteria of the landmark case, Central Hudson Gas & Electric Corp. v. Public
Service Commission. Under this case, commercial speech is not afforded First
Amendment protections if it is, "misleading or related to unlawful activity."

- Elements of False Advertising

♦ ​Elements of false advertising are the following:

-Unfair statements
An advertisement is deemed unfair when there is risk of harm to the consumer,
or if the results of a product's claim are disadvantageous
-Misleading claims
omission of material information, the disclosure of which is necessary to prevent
the claim, practice, or sale from being misleading
-Dishonesty
Dishonesty in advertising occurs when an advertiser offers such a guarantee or
other bonus and the consumer does not receive it.

-Unreasonable basis
An advertisement is considered unreasonable when the advertiser makes more
than one claim and at least one is deemed inaccurate or untrue.

- Ethical Considerations
It is possible for an advertisement to be legally permissible (in that it is truthful, not misleading
and supported by objective evidence), but for it to be unethical. Ethical considerations relate to
the manner in which the content is being delivered, the intended consumer demographic, and the
message of the advertisement.

♦ Advertisement of harmful or hazardous products

Advertising specific products to specific consumers can be deemed unethical. The classic
example of this is the advertisement of alcohol or tobacco to minors.

Typical harmful products include: tobacco, alcohol, lead paint, sugary snacks and drinks.
In general, harmful products may be loosely regarded as products which carry health or
safety risks, regardless of their method of intended consumption. For example, tobacco is
considered harmful due to its association with lung cancer. However, cleaning products
may also be considered harmful, as improper consumption may also lead to injury. In
general, harmful products may be advertised, but must be done so with full disclosure of
the risks involved in consumption.

♦ Methods of appeal to the consumer

An advertisement may be deemed unethical if it appeals to the user through

-Fear
-Sex
-Spiritual/religious arguments
-Unverified or exaggerated claims

False or misleading advertisements fall within the FTC Act’s jurisdiction and advertisers may face
criminal and civil penalties for such content. The same penalties may not apply for unethical
advertisements.

Source articles: ​https://yourbusiness.azcentral.com/elements-false-advertising-17209.html​ (starting point)

https://accountlearning.com/unethical-advertising-forms-effects-improve/

https://smallbusiness.chron.com/advertisement-harmful-products-65363.html
